from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth
from django.contrib.auth import authenticate
from django.contrib import messages
from account.models import Account
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    username = None
    if request.method == 'POST' :
        print('pumasok tangina dito')
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return redirect("home")
        else:
            print("INCORRECT")
            messages.info(request, 'invalid credentials')
            return redirect('/')
    else:
        print('dito pumasok tangina')
        return render(request,"index.html")

def home(request):
    if request.session.has_key('username'):
        return render(request, "home.html")
    else:
        redirect('index')

def not_verified(request):
    return render(request, "not_verified.html")

def complete_register(request):
    return render(request, "complete_register.html")

def login(request):
    username = None
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        #checks if username is empty
        if not username:
            messages.info(request, 'Username cannot be blank')
            return redirect('login')
        #if username not empty
        else:
            if not password:
                messages.info(request, 'Empty password')
                return redirect('login')
            else:
                user = auth.authenticate(username=username,password=password)
                if user is not None:
                    auth.login(request, user)
                    request.session['username'] = username
                    verified = user.is_verified                      
                    if verified:
                        complete_registered = user.is_reg_complete
                        if complete_registered:                       
                            return redirect('home')
                        else:
                            return redirect('complete_register')
                    else:           
                        return render(request, 'not_verified.html', {'username': username})
                else:
                    messages.info(request, 'The username or password is incorrect.')
                    return redirect('login')
    else:
        return render(request, 'index.html')

def logout_view(request):
    logout(request)
    return redirect('login')

def register(request):
    if request.method == 'POST':
        first_name = request.POST['first_name']
        middle_name = request.POST['middle_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        password = request.POST['password']
        password2 = request.POST['password2']
        month = request.POST['month']
        day = request.POST['day']
        year = request.POST['year']
        address1 = request.POST['address1']
        municipality = request.POST['municipality']
        province = request.POST['province']
        contact = request.POST['contact']
        email = request.POST['email']
        establishment = request.POST['establishment']
        nob = request.POST['nob']
        img = request.POST['img']

        print(first_name)
        print(middle_name)
        print(last_name)
        print(username)
        print(password)
        print(password2)
        print(month)
        print(day)
        print(year)
        print(address1)
        print(municipality)
        print(province)
        print(contact)
        print(email)
        print(establishment)
        print(nob)

        Account.objects.create_user(first_name=first_name, middle_name=middle_name, last_name=last_name, username=username, password=password, month=month, day=day, year=year, address1=address1, municipality=municipality, province=province, contact=contact, email=email, establishment=establishment, nob=nob)

        return redirect('login')

    else:
        return render(request, 'register.html')

def final_register(request):
    if request.method == 'POST':
        designation = request.POST['designation']
        company_address = request.POST['company_address']
        office_number = request.POST['office_number']
        where_did_you_learn = request.POST['where_did_you_learn']
        dietary_restrictions = request.POST['dietary_restrictions']
        is_subscribe = request.POST['is_subscribe']
        is_included_in_directory = request.POST['is_included_in_directory']

        print(designation)
        print(company_address)
        print(office_number)
        print(where_did_you_learn)
        print(dietary_restrictions)
        print(is_subscribe)
        print(is_included_in_directory)
        
        if 'username' in request.session:
            user = request.session['username']

        acc = Account.objects.get(username=user)
        print(acc)
        acc.designation=designation
        acc.company_address=company_address
        acc.office_number=office_number
        acc.where_did_you_learn=where_did_you_learn
        acc.dietary_restrictions=dietary_restrictions
        acc.is_subscribe=is_subscribe
        acc.is_included_in_directory=is_included_in_directory
        acc.is_reg_complete=True
        acc.save()
        return redirect('home')

    else:
        return render(request, 'complete_register.html')