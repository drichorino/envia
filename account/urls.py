from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='login'),
    path('register', views.register, name='register'),
    path('login', views.login, name='login'),
    path('logout_view', views.logout_view, name='logout_view'),
    path('not_verified', views.not_verified, name='not_verified'),
    path('complete_register', views.complete_register, name='complete_register'),
    path('final_register', views.final_register,name='final_register')
]