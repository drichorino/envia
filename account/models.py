from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.contrib.auth.hashers import check_password 
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File
from urllib.request import urlopen
from tempfile import NamedTemporaryFile
import os


# Create your models here.
class MyAccountManager(BaseUserManager):
    def create_user(self, first_name, middle_name, last_name, day, month, year, address1, municipality, province, contact, establishment, nob, username, email, password=None):
        if not email:
            raise ValueError("Users must have an email address")
        if not username:
            raise ValueError("Users must have a username")

        user = self.model(
            username=username,
            email=self.normalize_email(email),
            first_name=first_name,
            middle_name=middle_name,
            last_name=last_name,
            day=day,
            month=month,
            year=year,
            address1=address1,
            municipality=municipality,
            province=province,
            contact=contact,
            establishment=establishment,
            nob=nob,            
            )
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_user2(self, username, email, password=None):
        user2 = self.model(
            username=username,
            email=self.normalize_email(email),
            )
        user2.set_password(password)
        user2.save(using=self._db)
        return user2

    def create_superuser(self, email, username, password):
        user = self.create_user2(
            username=username,
            password=password,
            email=self.normalize_email(email),
            )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.is_verified = True
        user.save(using=self._db)
        return user

    def register(self, designation, company_address, office_number, where_did_you_learn, dietary_restrictions, is_subscribe, is_included_in_directory):
        user = self.model(
           designation=designation,
           company_address=company_address,
           office_number=office_number,
           where_did_you_learn=where_did_you_learn,
           dietary_restrictions=dietary_restrictions,
           is_subscribe=is_subscribe,
           is_included_in_directory=is_included_in_directory,
            )
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):    
    username = models.CharField(max_length=30, unique=True)
    first_name = models.CharField(max_length=30, blank=True)
    middle_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)    
    month = models.IntegerField(null=True)
    day = models.IntegerField(null=True)
    year = models.IntegerField(null=True)
    address1 = models.CharField(max_length=50, blank=True)
    municipality = models.CharField(max_length=20, blank=True)
    province = models.CharField(max_length=20, blank=True)
    mailing_address = models.CharField(max_length=100, blank=True)
    contact = models.CharField(max_length=11, blank=True)
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    establishment = models.CharField(max_length=50, blank=True)
    nob = models.CharField(max_length=1, blank=True)
    designation = models.CharField(max_length=50, blank=True)
    company_address = models.CharField(max_length=100, blank=True)
    office_number = models.CharField(max_length=11, blank=True)
    where_did_you_learn = models.IntegerField(null=True, blank=True)
    dietary_restrictions = models.CharField(max_length=50, blank=True)
    is_subscribe = models.BooleanField(default=False)
    is_included_in_directory = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    is_reg_complete = models.BooleanField(default=False)
    img = models.FileField(upload_to='images/',null=True, blank=True)
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = MyAccountManager()

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True