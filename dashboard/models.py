from django.db import models

# Create your models here.
class Module(models.Model):
    module_id = models.AutoField(primary_key=True)
    module_title = models.CharField(max_length=500)
    total_hours = models.PositiveIntegerField()


class Topic(models.Model):
    topic_id = models.AutoField(primary_key=True)
    module_fkid = models.ForeignKey(Module, on_delete=models.SET_DEFAULT, default=0)
    topic_title = models.CharField(max_length=500)
    hours = models.PositiveIntegerField()