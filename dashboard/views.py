from django.shortcuts import render
from .models import Module
from .models import Topic

# Create your views here.
def home(request):
    modules = get_modules()
    topics = Topic.objects.all()
    return render(request, "home.html", {'modules': modules, 'topics' : topics})

def get_modules():
    modules = Module.objects.all()
    return modules

def get_topics():
    topics = Topic.objects.all()
    return topics

def get_topics_based_on_module_id(module_id):
    topics = get_topics()
    top = {}
    for topic in topics:
        if module_id == topic.module_fkid_id:
            top.update( {'topic' : topic.topic_title} )
    return top
