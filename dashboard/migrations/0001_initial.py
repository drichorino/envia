# Generated by Django 3.0.7 on 2020-07-08 12:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Module',
            fields=[
                ('module_id', models.AutoField(primary_key=True, serialize=False)),
                ('module_title', models.CharField(max_length=500)),
                ('total_hours', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('topic_id', models.AutoField(primary_key=True, serialize=False)),
                ('topic_title', models.CharField(max_length=500)),
                ('hours', models.PositiveIntegerField()),
                ('module_fkid', models.ForeignKey(default=0, on_delete=django.db.models.deletion.SET_DEFAULT, to='dashboard.Module')),
            ],
        ),
    ]
