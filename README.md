# envia
PCO Online Training System

installed app in order to run (shown in requirements.txt as well)
python 3.8
pip
pipenv
django 3.0.7
gunicorn 20.0
whitenoise 5.1

Testing:
app deployed in enviasit.herokuapp.com

Important:
-Procfile is what heroku runs when the app is deployed
-requirement.txt is the list of application heroku will install once code is pushed
-whitenoise is used by django to handle statics (css, js, image, etc)